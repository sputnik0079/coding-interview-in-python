#!/usr/bin/env python


class Solution:
    """
    @param numbers : Give an array numbers of n integer
    @param target : you need to find four elements that's sum of target
    @return: Find all unique quadruplets in the array which gives the sum of 0.
    """
    def fourSum(self, numbers, target):
        if len(numbers) < 4:
            return []
        numbers.sort()
        result = []
        for i in xrange(len(numbers)):
            if i > 0 and numbers[i] == numbers[i-1]:
                continue
            for j in xrange(i+1, len(numbers)):
                if j > i+1 and numbers[j] == numbers[j-1]:
                    continue
                lo, hi = j+1, len(numbers)-1
                while lo < hi:
                    sol = [numbers[i], numbers[j], numbers[lo], numbers[hi]]
                    sums = sum(sol)
                    if sums == target:
                        result.append(sol)
                        lo, hi = lo+1, hi-1
                        while lo < hi and numbers[lo] == numbers[lo-1]:
                            lo += 1
                        while lo < hi and numbers[hi] == numbers[hi+1]:
                            hi -= 1
                    elif sums < target:
                        lo += 1
                    else:
                        hi -= 1
        return result

    def four_sum(self, numbers, target):
        """This solution will cause Time Limit Exceeded, but I like it."""
        if len(numbers) < 4:
            return []
        numbers.sort()
        result = set()
        for i in xrange(len(numbers)):
            for j in xrange(i+1, len(numbers)):
                lo, hi = j+1, len(numbers)-1
                while lo < hi:
                    sol = (numbers[i], numbers[j], numbers[lo], numbers[hi])
                    sums = sum(sol)
                    if sums == target:
                        result.add(sol)
                        lo, hi = lo+1, hi-1
                    elif sums < target:
                        lo += 1
                    else:
                        hi -= 1
        return list(result)
