#!/usr/bin/env/python


class Solution:
    def threeSum(self, numbers):
        '''Return a list of triplet which gives the sum of zero.
        '''
        if len(numbers) < 3:
            return []
        numbers.sort()
        result = set()
        for i in xrange(len(numbers)):
            lo, hi = i+1, len(numbers)-1
            while lo < hi:
                sol = (numbers[i], numbers[lo], numbers[hi])
                sums = sum(sol)
                if sums == 0:
                    result.add(sol)
                    lo, hi = lo+1, hi-1
                elif sums < 0:
                    lo += 1
                else:
                    hi -= 1
        return list(result)

    def three_sum(self, numbers):
        '''Return a list of triplet which gives the sum of zero.
        '''
        if not numbers or len(numbers) < 3:
            return []
        numbers.sort()
        result = []
        for i in xrange(len(numbers)):
            if i > 0 and numbers[i] == numbers[i-1]:
                continue
            lo, hi = i+1, len(numbers)-1
            while lo < hi:
                sol = [numbers[i], numbers[lo], numbers[hi]]
                sums = sum(sol)
                if sums == 0:
                    result.append(sol)
                    lo, hi = lo+1, hi-1
                    while lo < hi and numbers[lo] == numbers[lo-1]:
                        lo += 1
                    while lo < hi and numbers[hi] == numbers[hi+1]:
                        hi -= 1
                elif sums < 0:
                    lo += 1
                else:
                    hi -= 1
        return result
