#!/usr/bin/env python

class Solution:
    # @param nums: The integer array
    # @param target: Target number to find
    # @return the first position of target in nums, position start from 0
    def binarySearch(self, nums, target):
        if not nums:
            return -1
        result = -1
        lo, hi = 0, len(nums)
        while lo < hi:
            mid = (lo+hi) // 2
            val = nums[mid]
            if val == target:
                result = hi = mid
            elif val < target:
                lo = mid + 1
            else:
                hi = mid
        return result

    def binarySearch(self, nums, target):
        if not nums:
            return -1
        result = -1
        lo, hi = 0, len(nums)-1
        while lo <= hi:
            # Python uses an internal float type with native double precision
            mid = (lo+hi) // 2
            if nums[mid] == target:
                result = mid
                hi -= 1
            elif nums[mid] < target:
                lo += 1
            else:
                hi -= 1
        return result
