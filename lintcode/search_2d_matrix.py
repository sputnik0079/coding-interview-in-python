#!/usr/bin/env python

class Solution:
    """
    @param matrix, a list of lists of integers
    @param target, an integer
    @return a boolean, indicate whether matrix contains target
    """
    def searchMatrix(self, matrix, target):
        if not matrix:
            return False
        rows, cols = len(matrix), len(matrix[0])
        lo, hi = 0, rows*cols-1
        while lo < hi:
            mid = (lo+hi) // 2
            midval = matrix[mid/cols][mid%mid]
            if midval == target:
                return True
            elif midval < target:
                lo += 1
            else:
                hi -=1
        return False
