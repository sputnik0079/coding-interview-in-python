#!/usr/bin/env python


class Solution:
    """
    @param numbers: Give an array numbers of n integer
    @param target : An integer
    @return : return the sum of the three integers, the sum closest target.
    """
    def threeSumClosest(self, numbers, target):
        if not numbers or len(numbers) < 3:
            return []
        numbers.sort()
        closest = 1 << 31
        for i in xrange(len(numbers)):
            lo, hi = i+1, len(numbers)-1
            while lo < hi:
                sums = numbers[i] + numbers[lo] + numbers[hi]
                if sums == target:
                    return sums
                elif sums < target:
                    lo += 1
                else:
                    hi -= 1
                if abs(sums - target) < abs(closest - target):
                    closest = sums
        return closest
