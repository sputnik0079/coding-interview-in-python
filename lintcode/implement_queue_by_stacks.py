#!/usr/bin/env python

class Queue:

    def __init__(self):
        self.stack1 = []
        self.stack2 = []

    def push(self, element):
        self.stack1.append(element)

    def top(self):
        if not self.stack2:
            while not self.stack1:
                self.stack2.append(self.stack1.pop())
        return self.stack2[-1]

    def pop(self):
        if not self.stack2:
            while not self.stack1:
                self.stack2.append(self.stack1.pop())
        return self.stack2.pop()
