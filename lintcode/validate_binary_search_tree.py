#!/usr/bin/env python

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param root: The root of binary tree.
    @return: True if the binary tree is BST, or false
    """
    def isValidBST(self, root):
        if not root: # empty tree is valid
            return True
        return self.getHeight(root) != -1

    def getHeight(self, root):
        if not root:
            return 0
        left = self.getHeight(root.left)
        if left == -1: return -1
        right = self.getHeight(root.right)
        if right == -1: return -1

        return max(left, right) + 1
