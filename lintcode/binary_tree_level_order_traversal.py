#!/usr/bin/env/python


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""


class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        from collections import deque
        if not root:
            return []
        result = []
        queue = deque([root])
        while queue:
            level, count = [], len(queue)
            while count:
                node = queue.popleft()
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
                level.append(node.val)
                count -= 1
            result.append(level)
        return result
