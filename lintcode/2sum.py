#!/usr/bin/env/python


class Solution:
    """
    @param numbers : An array of Integer
    @param target : target = numbers[index1] + numbers[index2]
    @return : [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twosum(self, nums, target):
        """Returns a pair of index in a list in where the sum of two elements
        equals to the given target.
        """
        if len(nums) < 2:
            return [-1, -1]
        table = {}
        for index, elem in enumerate(nums):
            res = target - elem
            if res not in table:
                table[elem] = index
            else:
                return [min(table[elem]+1, index+1),
                        max(table[elem]+1, index+1)]
        return [-1, -1]
