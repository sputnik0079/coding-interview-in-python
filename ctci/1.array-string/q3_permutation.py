#!/usr/bin/env python

def ispermutation_table(s, t):
    """Returns True if two strings are permutation.

    Iterate over each element in string s, and keep tracking the occurence
    of each character. After that iterate over the string t, if the occurence
    are different, it is not a permutation.

    Time Complexity: O(N), where N means the number of characters in a string.
    Space Complexity: O(N) where N means the number of characters in a string.
    """
    if not s or not t:
        raise ValueError('Invalid input parameter.')
    if len(s) != len(t):
        return False
    table = {}
    for elem in s:
        table[elem] = table.get(elem, 0) + 1
    for elem in t:
        table[elem] = table.get(elem, 0) - 1
        if table[elem] < 0:
            return False
    return True

def ispermutation_set(s, t):
    """Returns True is two strings are permutation.

    As permutation are same characters in different order, we can convert
    them to set type then compare the results.
    Sorting before testing has the same effect, but sets don't reply on an
    expensive sort (time complexity is O(nlogn).

    Time Complexity: O(N)
    Space Complexity: O(N)
    """
    if not s or not t:
        raise ValueError('Invalid input parameters.')
    return set(s) == set(t)

if __name__ == '__main__':
    funclist = [ispermutation_table, ispermutation_set]
    testlist = [('g
