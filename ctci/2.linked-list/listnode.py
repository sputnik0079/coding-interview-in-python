#!/usr/bin/env python


class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        if next is not None:
            self.next = next
        else:
            self.next = None


class DListNode(object):
    def __init__(self, val, prev=None, next=None):
        self.val = val
        self.prev = prev if prev else None
        self.next = next if next else None


def slreverse(head):
    """Reverse a single linked list in-place"""
    if not head or not head.next:
        return head
    prev, curr, next = None, head, None
    while curr:
        next = curr.next
        curr.next = prev
        prev = curr
        curr = next
    return prev


def sldelete(head, target):
    if not head:
        return None
    result = ListNode(-1)
    result.next = head
    curr = result
    while head and head.next:
        if head.val == target:
            curr.next = head.next
        else:
            curr.next = head
            head = head.next
        curr = curr.next
    return result.next


def build(n, start=0, reverse=False):
    content = [i for i in xrange(start, n+start)]
    if reverse:
        content.reverse()
    result = ListNode(-1)
    curr = result
    for val in content:
        curr.next = ListNode(val)
        curr = curr.next
    return result.next


def printlst(head):
    curr = head
    while curr:
        print curr.val,
        curr = curr.next
    print

if __name__ == '__main__':
    print 'Hello'
    head = ListNode(1, ListNode(2))
    curr = sldelete(head, 1)
    while curr:
        print curr.val,
        curr = curr.next
