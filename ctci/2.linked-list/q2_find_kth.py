#!/usr/bin/env/python

from listnode import ListNode
from listnode import build

def nth2last(head, k):
    if not head or k < 0:
        raise ValueError("Invalid parameters.")
    curr, length = head, 1

    # calculate the length of linked list
    while curr:
        curr = curr.next
        length += 1
    print length,
    curr = head
    count = length - k
    while count > 0:
        curr = curr.next
        count -= 1
    return curr


if __name__ == '__main__':
    h1 = build(10)
    nth = nth2last(h1, 1)
    print nth.val
