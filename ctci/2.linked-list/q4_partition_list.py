#!/usr/bin/env/python

from listnode import ListNode
from listnode import build
from listnode import printlst

def partition(head, x):
    if not head:
        return None
    less = ListNode(-1)
    lo = less
    great = ListNode(-1)
    hi = great
    while head:
        if head.val < x:
            lo.next = head
            lo = lo.next
        else:
            hi.next = head
            hi = hi.next
        head = head.next
    lo.next = great.next
    hi.next = None
    return less.next
    
if __name__ == '__main__':
    h1 = build(10, 7, True)
    printlst(h1)
    h = partition(h1, 15)
    printlst(h)