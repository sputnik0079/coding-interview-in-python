#!/usr/bin/env/python

from listnode import ListNode


def remove_dups(head):
    if not head:
        return None
    curr, runner = head, None
    while curr:
        runner = curr.next
        while runner.next:
            if curr.val == runner.val:
                runner.next = runner.next.next
            else:
                runner = runner.next
        curr = curr.next
    return head


def remove_duplicates(head):
    if not head:
        return None
    curr, table = head, set()
    while curr and curr.next:
        if curr.val in table:
            curr.next = curr.next.next
        else:
            table.add(curr.val)
    return head


if __name__ == '__main__':
    h1 = ListNode(1, ListNode(1, ListNode(2, ListNode(3))))
    curr = h1
    while curr:
        print curr.val,
        curr = curr.next
    print
    curr = remove_dups(h1)
    while curr:
        print curr.val,
        curr = curr.next
    print

    h2 = ListNode(1, ListNode(1, ListNode(1)))
    curr = h2
    curr = remove_dups(h2)
    while curr:
        print curr.val,
        curr = curr.next
    print

    h3 = ListNode(1, ListNode(1))
    curr = remove_dups(h3)
    while curr:
        print curr.val,
        curr = curr.next
    print
