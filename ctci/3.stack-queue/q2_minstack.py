#!/usr/bin/env/python


class MinStack(object):
    def __init__(self):
        self._minstack = []
        self._stack = []

    def push(self, val):
        if val <= self.min():
            self._minstack.append(val)
        self._stack.append(val)

    def pop(self):
        if not self._stack:
            raise ValueError("underflow")
        val = self._stack.pop()
        if val == self.min():
            self._minstack.pop()
        return val

    def min(self):
        if not self._minstack:
            return float('inf')
        return self._minstack[-1]

    def top(self):
        if not self._stack:
            raise ValueError("underflow")
        return self._stack[-1]

    def __len__(self):
        return len(self._stack)


if __name__ == '__main__':
    minstack = MinStack()
    minstack.push(3)
    minstack.push(2)
    minstack.push(1)
    print minstack.min() == 1
    print minstack.pop()
    print minstack.min() == 2
