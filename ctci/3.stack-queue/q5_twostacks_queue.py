#!/usr/bin/env/python

class MyQueue(object):
    def __init__(self):
        self.enque = []
        self.deque = []

    def enqueue(self, val):
        self.enque.append(val)

    def dequeue(self):
        self.shift_queue()
        if not self.deque:
            raise ValueError("underflow")
        return self.deque.pop()

    def top(self):
        self.shift_queue()
        if not self.deque:
            raise ValueError("underflow")
        return self.deque[-1]

    def shift_queue(self):
        if not self.deque:
            while not self.enque:
                self.deque.append(self.enque.pop())

if __name__ == '__main__':
    que = MyQueue()
    que.enqueue(1)
    que.enqueue(2)
    print que.dequeue() == 1
    print que.top() == 2
