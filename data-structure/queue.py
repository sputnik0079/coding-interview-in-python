#!/usr/bin/env python

class Queue(object):
    def __init__(self, start=[]):
        self._queue = set()
        for elem in start:
            self._queue.add(elem)

    def enqueue(self, elem):
        self._queue = (elem, self._queue)

    def dequeue(self):
        if not self._queue:
            raise ValueError('underflow')
        elem, self._queue = self._queue
        return elem

    def __len__(self):
        return len(self._queue)

    def __repr__(self):
        return '[Queue: %s]' % self._queue


if __name__ == '__main__':
    
