#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Personal Implementation of Stack class.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""


class Stack(object):
    """
    Stack provides basic and standard methods as a stack, such like pop, push,
    peek(top).
    """

    def __init__(self, start=[]):
        """
        An optional *start* provides as an initial list.
        """
        self._stack = []
        for val in start:
            self.append(val)

    def push(self, val):
        """Push the element *val* to the end of the stack"""
        self._stack.append(val)

    def pop(self):
        """Fetch the last element then delete it."""
        return self._stack.pop()

    def top(self):
        """Return the last element in the stack"""
        if not self._stack:
            raise ValueError("underflow")
        return self._stack[-1]

    def __len__(self):
        """Return the number of elements in the stack"""
        return len(self._stack)

    def __getitem__(self, position):
        """Return the element at *position*"""
        return self._stack[position]

    def __repr__(self):
        """Return the string representation of Stack."""
        return '[Stack: %s]' % self._stack

if __name__ == '__main__':
    stack = Stack()
    stack.push("spam")
    stack.push(23)
    print stack
    print stack.top()
    print len(stack)
    print 23 in stack
