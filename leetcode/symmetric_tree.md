Symmetric Tree 
---------------

> Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
> 
> For example, this binary tree is symmetric:
>```
>       1
>      / \
>     2   2
>    / \ / \
>   3  4 4  3
>```
> But the following is not:
>```
>       1
>      / \
>     2   2
>      \   \
>       3   3
>```
> Note:
> 
> Bonus points if you could solve it both recursively and iteratively.

### Solution

We assume that empty tree is symmetric.

1. As Binary Tree is a repeated data structure, we can solve this problem by recursively check whether its left subtree is mirror of its right subtree based on the condition:
    + If both trees are empty, return True.
    + If one of them is empty and the other is not then return False.
    + Otherwise, if left.left is a mirror of right.right or left.right if a mirror of right.left, return True

2. We can also use a additional stack (`list` in Python) to iteratively visit each tree node and check them based on the same condition.

### Code

```python
def isSymmetric(root):
    if not root:
            return True
    return symmetric(root.left, root.right)

def symmetric(left, right):
    if not left and not right:
        return True
    if not left or not right:
        return False
    return left.val == right.val and \
        symmetric(left.left, right.right) and \
        symmetric(left.right, right.left)

def isSymmetric(root):
    if not root:
        return True
    stack = [root.left, root.right]
    while stack:
        rgt, lft = stack.pop(), stack.pop()
        if not lft and not rgt:
            continue
        if not lft or not rgt or lft.val != rgt.val:
            return False
        stack += [lft.left, rgt.right, lft.right, rgt.left]
    return True
```
