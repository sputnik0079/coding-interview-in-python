3Sum 
--------

> Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
> 
> **Note:**
> 
> + Elements in a triplet `(a,b,c)` must be in non-descending order. (ie, `a ≤ b ≤ c)`
> + The solution set must not contain duplicate triplets.
> 
> For example, given array S = `{-1 0 1 2 -1 -4}`, a solution set is: `(-1, 0, 1)` and `(-1, -1, 2)`

### Solution

The general idea is to apply a two indexes on the array, like in binary search, we use a `low` index from 0 and a `high` index in reversed order. To do this, we first need to sort the array in place, the time complexity is *O(nlogn)*.

After that, traverse the array and fix the element as `array[i]` where *i* is from `0` to `len(array) - 2`, within the loop, initialize two more indexes like low and high. If the result equals to zero, append the candidate elements onto the list, increase the low, decrease the high.

What's more, if duplicates are allowed, we can use a `set()` to filter the duplicates.
 
### Code

 ```python
def threeSum(num):
    if not num or len(num) < 3:
        return []
    num.sort() # sort the sequence in-place
    result = []
    for i in xrange(len(num)):
        if i-1 >= 0 and num[i-1] == num[i]:
            continue
        lo, hi = i+1, len(num)-1
        while lo < hi:
            sol = [num[i], num[lo], num[hi]]
            sums = sum(sol)
            if sums == 0:
                result.append(sol)
                lo, hi = lo+1, hi-1
                while lo < hi and num[lo] == num[lo-1]:
                    lo += 1
                while lo < hi and num[hi] == num[hi+1]:
                    hi -= 1
                elif sums < 0:
                    lo += 1
                else:
                    hi -= 1
    return result

def threeSumSet(num):
    if not num or len(num) < 3:
        return []
    result = set()
    for i in xrange(len(num) - 2):
        lo, hi = i+1, len(num)-1
        while lo < hi:
            solution = (num[i], num[lo], num[hi])
            sums = sum(solution)
            if sums == 0:
                result.add(solution)
                lo, hi = lo+1, hi-1
            elif sums < 0:
                lo += 1
            else:
                hi -= 1
    return result
 ```
