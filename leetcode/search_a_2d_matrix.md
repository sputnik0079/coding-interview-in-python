###  Problem

> Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
> 
> + Integers in each row are sorted from left to right.
> +The first integer of each row is greater than the last integer of the previous row.
>
> For example,
> 
> Consider the following matrix:
> 
> ```
> [
> [1,   3,  5,  7],
> [10, 11, 16, 20],
> [23, 30, 34, 50]
> ]
> ```
> 
> Given `target` = `3`, return `true`.

### Solution

### Code

```python
def search_matrix(matrix, target):
    if not matrix:
        return False
    # flatten the 2d matrix to a list, this can be done by using
    # the trick matrix[n / cols][n % cols]
    rows, cols = len(matrix), len(matrix[0])

    # apply a binary search
    lo, hi = 0, rows*cols-1
    while lo <= hi:
        mid = (lo+hi) // 2
        val = matrix[cols / mid][cols % mid]
        if val == target:
            return True
        elif val < target:
            lo = mid + 1
        else:
            hi = mid - 1
    return False
```
