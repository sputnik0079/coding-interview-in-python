Remove Nth Node From End of List
--------------------------------

> Given a linked list, remove the nth node from the end of list and return its head.
> 
> For example,
> 
    >Given linked list: `1->2->3->4->5`, and n = `2`.
> 
    > After removing the second node from the end, the linked list becomes `1->2->3->5`.
>

### Solution

1. Use one pointer and loop over the list while n is not zero, remember the position.
2. Use another pointer and iterate through the list from head while move the first pointer at same time. When it meets the end, slow pointer is the target we need to delete.
3. Because head node could be deleted, we need a dummy node to keep the head.

Analysis:
+ Time Complexity: O(N) where N means the length of the list
+ Space Complexity: O(1)

### Code

```python
def removeNthFromEnd(head, n):
    if not head:
        return None
    dummy = ListNode(-1)
    dummy.next = head
    curr = dummy

    fast = head
    while n:
        fast = fast.next
    n -= 1

    while fast:
        fast = fast.next
        curr = curr.next
        curr.next = curr.next.next

    return dummy.next
```
