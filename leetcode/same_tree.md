### Problem

> Given two binary trees, write a function to check if they are equal or not.
> 
> Two binary trees are considered equal if they are structurally identical and the nodes have the same value.

### Solution

### Code

```python
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = self.right = None

def is_same_tree(p, q):
    """Recursively compare two trees"""
    if not p and not q: # empty trees are same
        return True
    if not p or not q: # one of them is empty
        return False
    if p.val != q.val:
        return False
    return is_same_tree(p.left, q.left) and is_same_tree(p.right, q.right)
```

### Solution

### Code

```python
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = self.right = None

def is_same_tree(p, q):
    """Iteration"""
    if not p and not q:
        return True
    stack = [p, q]
    while stack:
        q, p = stack.pop(), stack.pop()
        if not p and not q:
            continue
        if not p or not q:
            return False
        if p.val != q.val:
            return False
        stack += [p.left, q.left, p.right, q.right]
    return True
```
