#!/usr/bin/env python

def mergesort(seq):
    if not seq:
        raise ValueError('Invalid sequence.')
    mid = len(seq) // 2
    lft, rgt = seq[:mid], seq[mid:]
    if len(lft) > 1:
        lft = mergesort(lft)
    if len(rgt) > 1:
        rgt = mergesort(rgt)
    i, j = 0, 0
    res = []
    while i < len(lft) and j < len(rgt):
        if lft[i] < rgt[j]:
            res.append(lft[i])
            i += 1
        else:
            res.append(rgt[j])
            j += 1
    res += lft[i:] + rgt[j:]
    return res


if __name__ == '__main__':
    lst1 = [1, 2, 5, 7, 3, 9]
    print mergesort(lst1) == sorted(lst1)
