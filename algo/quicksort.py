#!/usr/bin/env python

def partition(seq):
    pivot, seq = seq[0], seq[1:]
    lo = [x for x in seq if x <= pivot]
    hi = [x for x in seq if x > pivot]
    return lo, pivot, hi

def quicksort(seq):
    if len(seq) < 1:
        return seq
    lo, pi, hi = partition(seq)
    return quicksort(lo) + [pi] + quicksort(hi)

if __name__ == '__main__':
    lst1 = [1, 2, 5, 7, 3, 9]
    print quicksort(lst1) == sorted(lst1)    
