#!/usr/bin/env python

def partition(seq):
    pi, seq = seq[0], seq[1:]
    lo = [x for x in seq if x <= pi]
    hi = [x for x in seq if x > pi]
    return lo, pi, hi

def selectsort(seq, k):
    lo, pi, hi = partition(seq)
    m = len(lo)
    if m == k:
        return pi
    elif m < k:
        return selectsort(hi, k-m-1)
    else:
        return selectsort(lo, k)

if __name__ == '__main__':
    lst1 = [1, 2, 5, 7, 3, 9]
    print selectsort(lst1, 1) == sorted(lst1)
